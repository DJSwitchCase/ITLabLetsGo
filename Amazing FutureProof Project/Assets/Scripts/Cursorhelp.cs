using System;
using System.Collections;
using System.Collections.Generic;
using Lightbug.GrabIt;
using UnityEngine;
using UnityEngine.UI;

public class Cursorhelp : MonoBehaviour
{
    public Camera cam;
    //private Quaternion freezedRotation;
    public GameObject Player3D;
    public bool OnTriggerStayTest;
    public float distance;
    private Quaternion rotation;
    public float maxDegressPerSecond;
    public GameObject LookAt;

    public GameObject image1;

    public GameObject image2;

    public GameObject image3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        OnTriggerStayTest = true;
    }

    private void OnTriggerStay(Collider other)
    {
        
        if(Input.GetKey(KeyCode.E))
        {
            //cam.transform.LookAt(gameObject.transform);
            //cam.transform.rotation = Quaternion.LookRotation(LookAt.transform.position - cam.transform.position, transform.up);
            //cam.transform.rotation = Quaternion.LookRotation(-LookAt.transform.forward, transform.up);
            //cam.transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, Time.deltaTime * maxDegressPerSecond);

            //cam.transform.rotation = freezedRotation;
            Player3D.GetComponent<FirstPersonMovement>().speed = 0;
            Player3D.GetComponent<FirstPersonMovement>().enabled = false;
            Player3D.GetComponent<Jump>().enabled = false;
            Player3D.GetComponent<Crouch>().enabled = false;

            cam.GetComponent<FirstPersonLook>().sensitivity = 0;
            cam.GetComponent<GrabItFix>().enabled = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        if (Input.GetKey(KeyCode.F))
        {
            image1.GetComponent<Image>().enabled = true;
            image2.GetComponent<Image>().enabled = false;
            image3.GetComponent<Image>().enabled = false;
            Player3D.GetComponent<FirstPersonMovement>().speed = 1;
            Player3D.GetComponent<FirstPersonMovement>().enabled = true;
            Player3D.GetComponent<Jump>().enabled = true;
            Player3D.GetComponent<Crouch>().enabled = true;
            
            cam.GetComponent<FirstPersonLook>().sensitivity = 2;
            cam.GetComponent<GrabItFix>().enabled = true;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        OnTriggerStayTest = false;
    }
}
