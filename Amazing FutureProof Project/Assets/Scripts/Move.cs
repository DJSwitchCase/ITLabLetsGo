using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectToTeleport
{
    private GameObject obj;
    private Vector3 oldPos;
    public bool IsTeleported;
    private bool IsSeen;
    public bool IsKinematic;
    public float Zcoords;

    public ObjectToTeleport(GameObject obj, float zcoords)
    {
        this.obj = obj;
        IsTeleported = false;
        IsKinematic = obj.GetComponent<Rigidbody>().isKinematic;
        Zcoords = zcoords;
    }
    
    public void Teleport()
    {
        if (!IsTeleported && IsSeen && VendingMachineInteract.eArcadeButtonPressed)
        {
            setOldPos();
            obj.GetComponent<Rigidbody>().isKinematic = true;
            obj.transform.position = new Vector3(oldPos.x, oldPos.y, Zcoords);
            IsTeleported = true;
        }
        else if (IsTeleported && IsSeen && VendingMachineInteract.fArcadeButtonPressed)
        {
            IsTeleported = false;
            obj.transform.position = new Vector3(oldPos.x, oldPos.y, oldPos.z);
            if(!IsKinematic)
                obj.GetComponent<Rigidbody>().isKinematic = false;
        }
    }

    public void setOldPos()
    {
        oldPos = obj.transform.position;
    }

    public GameObject getObj()
    {
        return obj;
    }

    public void setIsSeen(bool val)
    {
        IsSeen = val;
    }
}
public class Move : MonoBehaviour
{
    public List<ObjectToTeleport> objsToTeleport;
    public GameObject projectionCam;
    public Camera cam;
    public float Zcoords;
    public Plane[] planes;
    public List<Collider> objCollider;
    public bool[] isTeleported;
    // Start is called before the first frame update
    void Start()
    {
        objsToTeleport = new List<ObjectToTeleport>(transform.childCount);
        for (int i = 0; i < transform.childCount; i++)
        {
            objsToTeleport.Add(new ObjectToTeleport(transform.GetChild(i).gameObject, Zcoords));
        }
        
        
        cam = projectionCam.GetComponent<Camera>();
        foreach(ObjectToTeleport obj in objsToTeleport)
            objCollider.Add(obj.getObj().gameObject.GetComponent<Collider>());
    }

    // Update is called once per frame
    void Update()
    {
        if (VendingMachineInteract.eArcadeButtonPressed || VendingMachineInteract.fArcadeButtonPressed)
        {
            planes = GeometryUtility.CalculateFrustumPlanes(cam);
            foreach (ObjectToTeleport obj in objsToTeleport)
            {
                if (GeometryUtility.TestPlanesAABB(planes, obj.getObj().gameObject.GetComponent<Collider>().bounds))
                {
                    obj.setIsSeen(true);
                }
                else
                {
                    obj.setIsSeen(false);
                }
            }

            //if (VendingMachineInteract.eArcadeButtonPressed || VendingMachineInteract.fArcadeButtonPressed)
            //{
            Teleportation();
           // }
        }
    }
    
    public void Teleportation()
    {
        foreach (ObjectToTeleport obj in objsToTeleport)
        {
            Debug.Log("Teleported!");
            obj.Teleport();
        }
        VendingMachineInteract.eArcadeButtonPressed = false;
        VendingMachineInteract.fArcadeButtonPressed = false;
    }
}
