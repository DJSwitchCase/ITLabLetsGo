using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class ButtonLogic : MonoBehaviour
{
    public GameObject image1;

    public GameObject image2;

    public GameObject image3;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PressTheButton()
    {
        if (SimpleCollectibleScript.CoinsCollected < 7)
        {
            image1.GetComponent<Image>().enabled = false;
            image2.GetComponent<Image>().enabled = true;
        }
        else
        {
            image1.GetComponent<Image>().enabled = false;
            image3.GetComponent<Image>().enabled = true;
        }
    }
}
