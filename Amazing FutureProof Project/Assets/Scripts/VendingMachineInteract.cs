using System;
using System.Collections;
using System.Collections.Generic;
using Lightbug.GrabIt;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class VendingMachineInteract : MonoBehaviour
{
    public Camera Player3dCamera;

    [FormerlySerializedAs("ProjectorCamera")]
    public Camera Player2DCamera;

    public GameObject Player3D;
    public GameObject Player2D;
    public static bool IsInArcadeMachine = false;
    public static bool IsPlayingArcade = false;
    public static bool eArcadeButtonPressed = false;
    public static bool fArcadeButtonPressed = false;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (Input.GetKey(KeyCode.E))
            {
                eArcadeButtonPressed = true;
                IsPlayingArcade = true;
                //Переключение камер
                Player3dCamera.GetComponent<Camera>().enabled = false;
                Player2DCamera.GetComponent<Camera>().enabled = true;

                //Отключение 3Д игроку управления, камеры и аудио
                Player3D.GetComponent<FirstPersonMovement>().enabled = false;
                Player3D.GetComponent<Jump>().enabled = false;
                Player3D.GetComponent<Crouch>().enabled = false;

                Player3dCamera.GetComponent<AudioListener>().enabled = false;
                Player3dCamera.GetComponent<FirstPersonLook>().enabled = false;
                Player3dCamera.GetComponent<GrabItFix>().enabled = false;

                //Включение 2D игроку управления, камеры и аудио
                Player2D.GetComponent<TwoDMovement>().enabled = true;
                Player2D.GetComponentInChildren<Jump>().enabled = true;
                Player2DCamera.GetComponent<AudioListener>().enabled = true;
                Player2D.GetComponent<Rigidbody>().isKinematic = false;
            }

            if (Input.GetKey(KeyCode.F))
            {
                fArcadeButtonPressed = true;
                IsPlayingArcade = false;
                //Переключение камер
                Player3dCamera.GetComponent<Camera>().enabled = true;
                Player2DCamera.GetComponent<Camera>().enabled = false;

                //Возвращение 3Д игроку управления, камеры и аудио
                Player3D.GetComponent<FirstPersonMovement>().enabled = true;
                Player3D.GetComponent<Jump>().enabled = true;
                Player3D.GetComponent<Crouch>().enabled = true;

                Player3dCamera.GetComponent<AudioListener>().enabled = true;
                Player3dCamera.GetComponent<FirstPersonLook>().enabled = true;
                Player3dCamera.GetComponent<GrabItFix>().enabled = true;

                //Выключение 2D игроку управления, камеры и аудио
                Player2D.GetComponent<TwoDMovement>().enabled = false;
                Player2D.GetComponent<Jump>().enabled = false;
                Player2DCamera.GetComponent<AudioListener>().enabled = false;
                Player2D.GetComponent<Rigidbody>().isKinematic = true;
            }
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            IsInArcadeMachine = true;
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            IsInArcadeMachine = false;
    }
}
