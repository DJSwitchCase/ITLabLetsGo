using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrosshairChange : MonoBehaviour
{
    public GameObject StandardCrosshair;
    public GameObject VendingMachineCrosshair;
    public GameObject NoneCrosshair;
    // Start is called before the first frame update
    void Start()
    {
        StandardCrosshair = gameObject.transform.GetChild(0).gameObject;
        VendingMachineCrosshair = gameObject.transform.GetChild(1).gameObject;
        NoneCrosshair = gameObject.transform.GetChild(2).gameObject;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Стандартный прицел
        if (VendingMachineInteract.IsInArcadeMachine == false && VendingMachineInteract.IsPlayingArcade == false)
        {
            StandardCrosshair.GetComponent<Image>().enabled = true;
            VendingMachineCrosshair.GetComponent<Image>().enabled = false;
            NoneCrosshair.GetComponent<Image>().enabled = false;
        }
        //Прицел во время игры в автомате
        else if (VendingMachineInteract.IsPlayingArcade)
        {
            StandardCrosshair.GetComponent<Image>().enabled = false;
            VendingMachineCrosshair.GetComponent<Image>().enabled = false;
            NoneCrosshair.GetComponent<Image>().enabled = true;
            
        }
        //Прицел у аркадного автомата
        else if (VendingMachineInteract.IsInArcadeMachine && VendingMachineInteract.IsPlayingArcade == false)
        {
            StandardCrosshair.GetComponent<Image>().enabled = false;
            VendingMachineCrosshair.GetComponent<Image>().enabled = true;
            NoneCrosshair.GetComponent<Image>().enabled = false;
        }
        
          
    }
}
